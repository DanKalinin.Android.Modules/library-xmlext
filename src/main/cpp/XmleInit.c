//
// Created by Dan on 18.10.2021.
//

#include "XmleInit.h"

jint JNI_OnLoad(JavaVM *vm, gpointer reserved) {
    xmle_init();

    return JNI_VERSION_1_2;
}

void JNI_OnUnload(JavaVM *vm, gpointer reserved) {

}
