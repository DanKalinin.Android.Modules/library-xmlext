cmake_minimum_required(VERSION 3.10.2)
project(library-xmlext C)

set(CMAKE_C_STANDARD 99)

file(GLOB C xml-ext/*.c)
file(GLOB L */lib/${ANDROID_ABI})
set(l libxml2.a)
set(I libxml2/include/libxml2)

add_library(library-xmlext SHARED ${C} XMLExt.h XmleMain.c XmleMain.h XmleInit.c XmleInit.h)
target_link_directories(library-xmlext PUBLIC ${L})
target_link_libraries(library-xmlext ${l} library-glibext)
target_include_directories(library-xmlext PUBLIC ${I} .)
